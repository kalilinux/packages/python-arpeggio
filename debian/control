Source: python-arpeggio
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>  
Uploaders: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 11), dh-python, python-all, python-setuptools, python3-all, python3-setuptools
Standards-Version: 4.1.4
Homepage: https://github.com/igordejanovic/Arpeggio
Vcs-Git: git://git.kali.org/packages/python-arpeggio.git
Vcs-Browser: http://git.kali.org/gitweb/?p=packages/python-arpeggio.git;a=summary
Testsuite: autopkgtest-pkg-python

Package: python-arpeggio
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}
Suggests: python-arpeggio-doc
Description: Parser interpreter based on PEG grammars (Python 2)
 This package contains a recursive descent parser with memoization based on PEG
 grammars (aka Packrat parser).
 .
 This package installs the library for Python 2.

Package: python3-arpeggio
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-arpeggio-doc
Description: Parser interpreter based on PEG grammars (Python 3)
 This package contains a recursive descent parser with memoization based on PEG
 grammars (aka Packrat parser).
 .
 This package installs the library for Python 3.

Package: python-arpeggio-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Parser interpreter based on PEG grammars (common documentation)
 This package contains a recursive descent parser with memoization based on PEG
 grammars (aka Packrat parser).
 .
 This is the common documentation package.
